const fs = require('fs')
const { name } = require('./package.json')
const request = require('request')
const { join, basename, dirname } = require('path')

module.exports = async (job, settings, { input, uploadUrl, rlink }, type) => {
  if (type != 'postrender') {
    throw new Error(`Action ${name} can be only run in postrender mode, you provided: ${type}.`)
  }
  if (!input || !uploadUrl || !rlink) {
    const nullParams = ''
    if (!input) nullParams += 'input '
    if (!uploadUrl) nullParams += 'uploadUrl '
    if (!rlink) nullParams += 'rlink '
    throw new Error(`Action ${name} error: ${nullParams}parameters are mandatory`)
  }
  const renderedFile = join(job.workpath, input)
  if (!fs.existsSync(renderedFile)) {
    throw new Error(`Action ${name} error: input file (${renderedFile}) does not exists`)
  }


  const uploadF = (_file) =>{
    const formData = {
      videoFile: fs.createReadStream(_file)
    };
    return new Promise(function (resolve, reject) {
      console.log('UPLADING VIDEO '+_file+', RLINK:', rlink)
      request.post({ url: uploadUrl + (_file.endsWith('.frag.mp4') ? '/frag/' : _file.endsWith('.mp4') ? '/mp4/' : '/webm/') + rlink, formData: formData }, function optionalCallback(err, httpResponse, body) {
        if (err) {
          reject(err)
          return console.error('upload failed:', err);
        }
        console.log('Upload successful! Server responded with:', body);
        resolve()
      });
    }).catch((error) => {
      throw error
    })
  }

  try{
    if(job.type == 'join'){
      const webmFile = join( dirname(renderedFile), basename(renderedFile,'.mp4') + '.webm' )
      if(fs.existsSync(webmFile)) await uploadF(webmFile)
      const fragFile = join( dirname(renderedFile), basename(renderedFile,'.mp4') + '.frag.mp4' )
      if(fs.existsSync(fragFile)) await uploadF(fragFile)
    }
    await uploadF(renderedFile)
  }catch(e){
    throw e
  }

}
