const { createClient } = require('@nexrender/api')
const { init, render } = require('@nexrender/core')
const { getRenderingStatus } = require('@nexrender/types/job')

const NEXRENDER_API_POLLING = process.env.NEXRENDER_API_POLLING || 30 * 1000;

/* TODO: possibly add support for graceful shutdown */
let active = true;

const delay = amount => (
  new Promise(resolve => setTimeout(resolve, amount))
)

const nextJob = async (client, settings) => {
  do {
    try {
      //console.log('settings:',settings)
      const job = await client.pickupJob();

      if (job && job.uid) {
        return job
      }
    } catch (err) {
      /* if (settings.stopOnError) {
        throw err;
      } else {
        console.error(err)
      } */
      console.error(err)
    }

    await delay(settings.polling || NEXRENDER_API_POLLING)
  } while (active)
}

/**
* Starts worker "thread" of continious loop
* of fetching queued projects and rendering them
* @param  {String} host
* @param  {String} secret
* @param  {Object} settings
* @return {Promise}
*/
const start = async (host, secret, settings) => {
  settings = init(Object.assign({}, settings, {
    logger: console,
  }))

  const client = createClient({ host, secret });

  do {
    let job = await nextJob(client, settings); {
      job.state = 'started';
    }

    try {
      const jobServerSide1 = await client.updateJob(job.uid, job)
      //console.log('after started, updatejob res:', jobServerSide1)
    } catch (err) {
      console.log(`[${job.uid}] error while updating job state to ${job.state}. Job abandoned.`)
      console.log(`[${job.uid}] error stack: ${err.stack}`)
      continue;
    }

    try {
      job.onRenderProgress = function (job, progress) {
        //console.log('onRenderProgress')
        try {
          /* send render progress to our server */
          //console.log('onRenderProgress', { uid: job.uid, state: job.state, error: job.error, template: job.template })
          const jobServerSide2 = client.updateJob(job.uid, getRenderingStatus(job))
          //console.log('onRenderProgress, updatejob res:', jobServerSide2)
        } catch (err) {
          /* if (settings.stopOnError) {
            throw err;
          } else {
            console.log(`[${job.uid}] error occurred: ${err.stack}`)
          } */
          console.log(`[${job.uid}] error occurred1: ${err}`)
        }
      }

      job = await render(job, settings); {
        job.state = 'finished';
      }
      //console.log('after finished',{ uid: job.uid, state: job.state, error: job.error, template: job.template })
      const rs = getRenderingStatus(job)
      //console.log('renderingStatus:',rs)
      await client.updateJob(job.uid, rs)
    } catch (err) {
      job.state = 'error';
      job.error = err;

      await client.updateJob(job.uid, getRenderingStatus(job));

      /* if (settings.stopOnError) {
        throw err;
      } else {
        console.log(`[${job.uid}] error occurred: ${err.stack}`)
      } */
      console.log(`[${job.uid}] error occurred2: ${err.stack}`,job)
    }
  } while (active)
}

module.exports = { start }
