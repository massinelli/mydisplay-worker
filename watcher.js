const chalk = require('chalk');
const { spawn, exec } = require('child_process');
const fs = require('fs');
const FSP = fs.promises
const {platform,cpus} = require("os");
const config = require('./config.json')
const request = require('request');
const { Console } = require('console');
const {join,basename} = require('path')

if (platform == "win64" || platform == "win32") {
  exec('net session', function (err, stdout, stderr) {
    
    if(err) {
      console.error(chalk.red('PLEASE RUN THIS APPLICATION WITH ADMINISTRATIVE PRIVILEGES!'))
      process.exit[0]
    }

    const checkFonts = async ()=>{
      console.log(chalk.cyan('CHECKING FONTS'))
      //try{
        if(!fs.existsSync('fonts')) await FSP.mkdir('fonts')
        const installedFonts = (await fs.promises.readdir(join(process.env.WINDIR || 'C:\\Windows', 'Fonts')))
          .filter(fileName => fileName.endsWith('.ttf') && !fileName.toUpperCase().includes('VARIABLEFONT'))
        //console.log(chalk.bgYellow.blackBright(installedFonts.join(' - ')))
        //console.log('-4')
        const unzip = async ()=>{
          if(!fs.existsSync('fonts/fonts.zip')) throw new Error('fonts.zip does not exists')
          const command = `7z.exe e fonts\\fonts.zip -ofonts`
          await new Promise(resolve=>{
            exec(command, (e,s,b) => {
              if (!e) return resolve()
              throw e
            })
          }).then(()=>FSP.unlink(join('fonts','fonts.zip')).catch(e=>console.log(chalk.red('can\'t remove fonts.zip'),e)))
        }
        //console.log('-3')
        const downloadFonts = ()=> new Promise((resolve, reject)=> {
          console.log(chalk.green('DOWNLOADING FONTS...'))
          const zippedFonts = fs.createWriteStream('fonts/fonts.zip')
          request
          .get({ url: `${config.apiUrl}${config.apiPort?':'+config.apiPort:''}` + '/fonts/'+ config.secret})
          .pipe(zippedFonts)
          .on('finish',(boh)=>{
            //console.log(chalk.redBright('FINISH DOWNLOAD, boh:',boh))
            //exec('dir',(e,s)=>console.log(chalk.yellow(s)))
            resolve()
          })
          .on('error',e=>{
            console.error(chalk.red('error downloading fonts as zipfile:'),e)
          })
        }).catch((error) => {
          console.error(chalk.red('error'),error)
        })
        //console.log('-2')
        const getFontsList = () => new Promise((resolve, reject)=>{
          console.log(chalk.green('GETTING FONTS LIST...'))
          request.get({ url: `${config.apiUrl}${config.apiPort?':'+config.apiPort:''}` + '/fonts/list/'+ config.secret}, (err, httpResponse, body) => {
            if (err) {
              console.error(chalk.red('get fonts list fail:'), err)
              return reject(err.code)
            }
            try{
              resolve(JSON.parse(body))
            }catch(e){
              reject(e)
            }
            
          })
        }).catch((error) => {
          throw error
        })
        //console.log('-1')
        const installFonts = async ()=>{
          const instF = (fontName) => new Promise((resolve, reject) => {
            if(!fontName.endsWith('.ttf')) return
            try{
              exec(`copy "${join('fonts',fontName)}" "%WINDIR%\\Fonts"`, ((err, x) => {
                if (err) return reject(err)
                exec(
                  `reg add "HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Fonts" /v "${basename(fontName.replace(/-|_/,' '), '.ttf')} (TrueType)" /t REG_SZ /d "${fontName}" /f`,
                  (err2, x2) => {
                    if (err2) return reject(err2)
                    console.log(chalk.magentaBright('FONT INSTALLED:', fontName))
                    setTimeout(resolve, 200)
                  }
                )
              }))
            }catch(e){
              console.log(chalk.red('can not install '+fontName+' font'),e)
            }
          })
          const fonts = await fs.promises.readdir('fonts')
          for(const font of fonts){
            if(installedFonts.includes(font)) {
              //console.log(chalk.blue('FONT '+font+' already installed'))
              continue
            }
            await instF(font).catch(e=>{
              console.error(chalk.red('error installing '+font+' font:'),e)
            })
          }
        }

        //console.log('1')
        const fontList = await getFontsList()
        //console.log('2')
        let needToDownload = false
        for (const fontName of fontList) {
          //console.log(chalk.bgBlue.cyan('fontName'),fontName)
          if (installedFonts.includes(fontName)) {
            //console.log(chalk.magenta('FONT ' + fontName + ' already installed'))
            continue
          }
          needToDownload = true
        }
        //console.log('3')

        if(!needToDownload) {
          console.log(chalk.cyan('all required fonts are already installed, go ahead'))
          return
        }
        //console.log('4')
        await new Promise((resolve,reject)=>{
          FSP.readdir('fonts')
          .then(fontsInFolder=>
            Promise.all(
              [
                ...fontsInFolder
                .filter(fif=>fif.endsWith('.ttf'))
                .map(fif => FSP.unlink(join('fonts',fif)))
              ]
            )
          ).then(resolve)

        })
        //console.log('5')
        await downloadFonts()
        //console.log('6')
        await unzip()
        //console.log('7')
        await installFonts()
      /* }catch (e){
        console.log(chalk.red('error checking fonts'),e)
      } */
      
    }

    let active = false
    const watcher = async ()=>{
      
      try{

        await checkFonts().catch(e => 
          console.log(chalk.red('error checking fonts'),e)
        )

        let fontsInterval = setInterval(()=>checkFonts().catch(e => console.log(chalk.red('error checking fonts'),e)),1000*60*30)

        console.log(chalk.bgBlue.whiteBright('\n Starting Worker \n'))
        active = true
        let maxThreads
        if(config.max_threads=='auto') maxThreads = cpus().length
        else{
          try{
            let threads=parseInt(config.max_threads)
            
            if(threads>0) maxThreads = threads
          }catch(e){
            maxThreads = cpus().length
          }
        }
        console.log('maxThreads:',maxThreads)
        const workerParameters = [
          '--trace-warnings',
          'src/bin.js',
          `--host=${config.nexRenderUrl}${config.nexRenderPort?':'+config.nexRenderPort:''}`,
          '--secret='+config.secret,
          '--workpath=temp',
          '--polling', config.polling,
          '--max_threads', maxThreads,
          '--worker_dir', __dirname
        ]
        if(!config.use_free_license) workerParameters.push('--no-license')
        let worker = spawn('node', workerParameters)
        worker.stdout.on('data', (data) => {
          console.log(`${data}`);
        });
        
        worker.stderr.on('data', (data) => {
          console.error(`ERROR: ${data}`);
        });
        
        worker.on('close', (code) => {
          active = false
          clearInterval(fontsInterval)
          console.log(`WORKER EXITED WITH CODE ${code}`);
        });

      }catch(e){

        active = false
        //if(!!fontsInterval) clearInterval(fontsInterval)
        console.log(`WATCHER EXCEPTION`,e);

      }
    }

    setInterval(()=>{
      if(active) return
      watcher()
    },10000)

    watcher()
    
  })
    
} else {
  console.log(chalk.red('this program can only run on windows'))
  process.exit(1);
}